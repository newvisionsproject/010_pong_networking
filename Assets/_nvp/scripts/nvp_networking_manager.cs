﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nvp_networking_manager : MonoBehaviour {

	// +++ editor fields ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	[SerializeField] GameObject _playerPrefab;
	[SerializeField] float _speed;




	// +++ life cycle +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	void Start () {

		this.Connect();
	}
	
	void Update () {
		
	}




	// +++ custom functions +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	private void InstatiatePlayer(){
		Debug.Log(PhotonNetwork.room.PlayerCount);

		if(PhotonNetwork.room.PlayerCount == 1){
			PhotonNetwork.Instantiate("Player", new Vector3(-7.5f, 0.0f, 0.0f), Quaternion.identity,0);
		}
		else{
			PhotonNetwork.Instantiate("Player", new Vector3(7.5f, 0.0f, 0.0f), Quaternion.identity, 0);
		}
		
	}

  private static void JoinOrCreateRoom()
  {
    RoomOptions ro = new RoomOptions();
    ro.MaxPlayers = 2;

    bool result = PhotonNetwork.JoinOrCreateRoom("nvp1", ro, TypedLobby.Default);
  }	


  private static void JoinLobby()
  {
    PhotonNetwork.JoinLobby();
  }

	void Connect(){
		bool result = PhotonNetwork.ConnectUsingSettings("v4.2");

		Debug.Log("Connected: " + result.ToString());

		if(!result) Debug.LogError("Connect: Error");
	}




	// +++ event handler ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	void OnConnectedToMaster()
  {
    Debug.Log("Success: OnConnectedToMaster");

		JoinLobby();
  }

  void OnJoinedLobby()
  {
    Debug.Log("Success: OnJoinedLobby");

    JoinOrCreateRoom();
  }

  void OnCreatedRoom(){
		Debug.Log("Success: OnCreatedRoom");
	}

	void OnJoinedRoom(){
		Debug.Log("Success: OnJoinedRoom");

		InstatiatePlayer();
	}




	// +++ failed event handlers ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	void OnPhotonRandomJoinFailed(){	
		Debug.LogError("Connect: OnPhotonRandomJoinFailed");
	}

	void OnPhotonJoinRoomFailed(){		
		Debug.LogError("Connect: OnPhotonJoinRoomFailed");
	}

	void OnFailedToConnectToPhoton(){
		Debug.LogError("Connect: OnFailedToConnectToPhoton");
	}

	void OnConnectionFail(){		
		Debug.LogError("Connect: OnConnectionFail");
	}

	void  OnPhotonMaxCccuReached(){
		Debug.LogError("Connect: OnPhotonMaxCccuReached");
	}
}
