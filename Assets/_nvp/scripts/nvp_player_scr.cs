﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nvp_player_scr : MonoBehaviour
{

  // +++ editor fields ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  [SerializeField] float _speed;

	public PhotonView _photonView;
	private Vector3 _remotePosition;

  void Start()
  {
		
		_remotePosition = transform.position;
  }

  void Update()
  {
		if(_photonView.isMine){
    	transform.Translate(Vector3.up * _speed * Input.GetAxis("Vertical") * Time.deltaTime, Space.World);
		}
		else {
			transform.position = Vector3.Lerp(transform.position, _remotePosition, Time.deltaTime*10);
		}
  }

  // +++ event hanlder ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    if (stream.isWriting)
    {
      //We own this player: send the others our data
			stream.SendNext(transform.position);
      
    }
    else
    {
      //Network player, receive data
      _remotePosition = (Vector3)stream.ReceiveNext();
    }
  }
}
